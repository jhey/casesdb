import { RouteConfig } from 'vue-router/types/router';

export const HomeRoutes: RouteConfig[] = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ './Home/Home.vue').then((m: any) => m.default),
  },
  {
    path: '/map',
    name: 'map',
    component: () => import(/* webpackChunkName: "map" */ './Map/Map.vue').then((m: any) => m.default),
  },
  {
    path: '/create',
    name: 'create',
    component: () => import(/* webpackChunkName: "create" */ './Create/Create.vue').then((m: any) => m.default),
  },
  {
    path: '/update',
    name: 'update',
    component: () => import(/* webpackChunkName: "update" */ './Update/Update.vue').then((m: any) => m.default),
  },
];
